#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
/* Read text from the socket and print it out. Continue until the
socket closes. Return nonzero if the client sent a "quit"
message, zero otherwise. */
int main(int argc, char *const argv[])
{
	FILE * fifo = fopen(argv[1], "r"); 
    int client_sent_quit_message;

	while (1)
    {
        char *text;
        int length;

        if (fscanf(fifo, "%d", &length) == 0) {
            continue;
        }

        text = (char *) malloc(length);

        fscanf(fifo, "%s", text);
        
        printf("%s\n", text);

        if (strcmp("quit", text)==0){
            free(text);
            break;
        } 
        
        free(text);

    }
	/* Remove the socket file. */
    fclose(fifo);
	return 0;
}
