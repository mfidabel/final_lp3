#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <fcntl.h>
/* Write TEXT to the socket given by file descriptor SOCKET_FD. */
void write_text (int socket_fd, const char* text)
{
	/* Write the number of bytes in the string, including
	NUL-termination. */
	int length = strlen (text) + 1;
	write (socket_fd, &length, sizeof (length));
	/* Write the string. */
	write (socket_fd, text, length);
}
int main (int argc, char* const argv[])
{
	const char* const socket_name = argv[1];
	const char* const message = argv[2];
	int socket_fd;
	/* Create the socket. */
	socket_fd = open(socket_name, O_WRONLY);
	/* Write the text on the command line to the socket. */
	write_text (socket_fd, message);
	close (socket_fd);
return 0;
}
