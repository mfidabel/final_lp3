//MATEO FIDABEL - FILA 3 - PRIMER PARCIAL
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

int infecciones_restantes;
int infeccion_maxima;
int tiempo_maximo;

pthread_mutex_t mutex_restantes = PTHREAD_MUTEX_INITIALIZER;

void* agente(void* arg){
  int mi_numero_agente = (int) arg, continuar = 1;

  while(continuar){
    int tiempo = (rand()%tiempo_maximo) + 1;

    sleep( tiempo ); //Esperamos guau que infecte

    //Revisamos si podemos infectar
    pthread_mutex_lock(&(mutex_restantes)); //Bloquea

    if (infecciones_restantes > 0) {
      //Infectamos
      infecciones_restantes--;
      printf("\nAgente #%d: Infecto a una persona luego de %d segundos.", mi_numero_agente, tiempo);
      printf("(Total contagios: %d)", infeccion_maxima-infecciones_restantes);
    } else {
      //Dejamos de infectar
      continuar = 0;
    }

    pthread_mutex_unlock(&(mutex_restantes)); //Desbloquea
  }

  return 0;
}

int main(int argc, char* argv[]) {
  int numero_agentes = atoi(argv[1]);
  tiempo_maximo = atoi(argv[2]);
  infeccion_maxima = atoi(argv[3]);
  infecciones_restantes = infeccion_maxima;

  //Seteamos semilla
  srand((long)time(NULL));

  //Iniciamos a contar tiempo
  time_t inicio_tiempo = time(NULL);

  //Creamos agentes
  pthread_t agentes[numero_agentes];
  printf("\nLos agentes se sueltan.");
  for (int i=1; i<=numero_agentes; i++){
    //Creamos cada hilo
    pthread_create(&(agentes[i-1]), NULL, &agente, (void *) i);
  }

  for (int i=0; i<numero_agentes; i++){
    //Creamos cada hilo
    pthread_join(agentes[i], NULL); //Ignoramos el return 
  }

  time_t fin_tiempo = time(NULL);

  printf("\nSe contagiaron a %d personas despues de %ld segundos\n", infeccion_maxima, (fin_tiempo - inicio_tiempo) ); //Ignoro el tipo long

  return 0;
}