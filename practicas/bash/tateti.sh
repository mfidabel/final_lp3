#!/bin/bash
#https://tldp.org/LDP/abs/html/writingscripts.html

creartablero () {
    TABLERO=("?" "?" "?" "?" "?" "?" "?" "?" "?" )
    SIMBOLO=("O" "X")
    JUGADOR=$1
    FIN=0
    GANADOR="NADIE"
}

hacermovimiento () {
    # $1 debe empezar en 0
    FILA=$(( $1 - 1 ))
    COLUMNA=$(( $2 - 1 ))
    INDICE=$(( $COLUMNA + ($FILA * 3) ))
    # Verificar si se marcó
    if [ "${TABLERO[$INDICE]}" == "?" ]; then
        # Esta disponible, escribo el simbolo
        TABLERO[$INDICE]=${SIMBOLO[$JUGADOR]}
        # Cambio de jugador
        JUGADOR=$(( 1 - $JUGADOR ))
    else
        echo "Posición Invalida"
    fi
}

imprimirtablero () {
    echo " ${TABLERO[0]} | ${TABLERO[1]} | ${TABLERO[2]}"
    echo "-----------"
    echo " ${TABLERO[3]} | ${TABLERO[4]} | ${TABLERO[5]}"
    echo "-----------"
    echo " ${TABLERO[6]} | ${TABLERO[7]} | ${TABLERO[8]}"
    echo ""
    echo "Your move, human (row, column)? "
}

verificarraya () {
    # Verificar que se cumpla esos tres $1 $2 $3
    if [ ${TABLERO[$1]} != "?" ] && [ ${TABLERO[$2]} == ${TABLERO[$1]} ] && [ ${TABLERO[$2]} == ${TABLERO[$3]} ]; then
        FIN=1
        GANADOR=${TABLERO[$1]}
    fi
}

verificarlleno () {
    # Verifica si hay espacios libres
    HAY=0
    for i in 0 1 2 3 4 5 6 7 8
    do
        if [ ${TABLERO[$i]} == "?" ]; then
            HAY=$(( $HAY + 1 ))
        fi
    done

    if [ $HAY -eq 0 ]; then
        FIN=1
    fi
}

verificarfin () {
    # Probamos por cada combinacion
    # Horizontal
    verificarraya 0 1 2
    verificarraya 3 4 5
    verificarraya 6 7 8
    # Vertical
    verificarraya 0 3 6
    verificarraya 1 4 7
    verificarraya 2 5 8
    # Diagonales
    verificarraya 0 4 8 
    verificarraya 6 4 2
    # Tablero completo
    verificarlleno
}

movimientobot () {
    # Buscamos un espacio
    # EMPEZAMOS CON OFFSET
    OFFSET=$(( RANDOM % 9 ))
    for i in 0 1 2 3 4 5 6 7 8
    do
        INDICE=$(( ($OFFSET + $i) % 9))
        if [ ${TABLERO[$INDICE]} == "?" ]; then
            hacermovimiento 1 $(( $INDICE + 1 ))
            break
        fi
    done
}

# Seleccionar primer turno
echo "Seleccione el primer turno: "
select OPCION in "Bot" "Jugador"
do
    case $OPCION in
        Bot)
            JUGADOR=1
            break
            ;;
        Jugador)
            JUGADOR=0
            break
            ;;
        *)
            echo "Introduzca una opción valida"
    esac
done

# Inicializamos el tablero

creartablero $JUGADOR

# Empezamos el juego

while true; do
    if [ $JUGADOR -eq 0 ]; then
        # JUGADOR FISICO
        imprimirtablero
        read ROW COLUMN
        hacermovimiento ROW COLUMN
    else
        # BOT
        movimientobot
    fi
    verificarfin
    if [ $FIN -eq 1 ]; then
        break;
    fi
done

imprimirtablero

echo "Ganador es: $GANADOR"