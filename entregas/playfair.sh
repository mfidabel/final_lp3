#!/bin/bash
declare -a CUADRADO
declare -a CODIGO
declare -a SPLIT
RESULTADO=""
ABECEDARIO=$( echo "ABCDEFGHIKLMNOPQRSTUVWXYZ" | fold -w 1 )

crearcuadrado () {
    CYPHER=$1
    # Quitamos letras repetidas y separamos en letras
    CODIGO=$( echo "$CYPHER" | sed -E ':a; s/(([[:alpha:]]).*)\2/\1/i; ta' | fold -w 1 )
    for letra in $CODIGO;
    do
        CUADRADO+=($letra)
    done;
    # Las siguientes letras cargamos al cuadrado si no estan puestas
    for letra in $ABECEDARIO;
    do
        if [[ " ${CUADRADO[*]} " != *" $letra "* ]]; then
            CUADRADO+=($letra)
        fi
    done
}

imprimircuadrado () {
    # funcion auxiliar
    for letra in ${CUADRADO[*]};
    do 
        echo "$letra"
    done
}

split () {
    # Dividimos las palabras, es igual de ambos lados
    # Reemplazamos los dos caracteres que se piden
    # eliminarmos los espacios y pares duplas
    SPLIT=$( echo "$1" | sed 's/ //g' | sed 's/J/I/g' | sed 's/Ñ/N/g' | fold -w 2 )
}

index () {
    # Busca el indice en el cuadrado de una letra
    for i in ${!CUADRADO[*]};
    do
        if [ "${CUADRADO[$i]}" == "$1" ]; then
            echo "$i"
            break;
        fi
    done
}


cifrar () {
    for digrama in $SPLIT; 
    do
        indxDER=0
        # Ver si el diagrama no tiene una sola letra
        if [ ${#digrama} -eq 1 ]; then
            indxDER=$( index "X" )
        else
            indxDER=$( index "${digrama:1:1}" )
        fi
        # Hallar index
        indxIZQ=$( index "${digrama:0:1}" )
        
        # Veremos cual es su fila y columna
        FILA1=$(( ($indxIZQ) / 5 ))
        FILA2=$(( ($indxDER) / 5 ))
        COLUMNA1=$(( ($indxIZQ) % 5 ))
        COLUMNA2=$(( ($indxDER) % 5 ))
        
        # AMBAS FILAS
        if [ $FILA1 -eq $FILA2 ]; then
            # Shiftear a la derecha
            COLUMNA1=$(( ($COLUMNA1 + 1) % 5 ))
            COLUMNA2=$(( ($COLUMNA2 + 1) % 5 ))
        # AMBAS COLUMNAS
        elif [ $COLUMNA1 -eq $COLUMNA2 ]; then
            FILA1=$(( ($FILA1 + 1) % 5 ))
            FILA2=$(( ($FILA2 + 1) % 5 ))
        # ESQUINAS
        else
            #INTERCAMBIAMOS DE ESQUINAS, MANTENER FILA PERO CAMBIAR COLUMNA
            aux=$COLUMNA1
            COLUMNA1=$COLUMNA2
            COLUMNA2=$aux
        fi

        indxIZQ=$(( ($FILA1 * 5) + $COLUMNA1 ))
        indxDER=$(( ($FILA2 * 5) + $COLUMNA2 ))
        # Agregamos al resultado
        RESULTADO="$RESULTADO${CUADRADO[$indxIZQ]}${CUADRADO[$indxDER]}"
    done
}

decifrar () {
    # Lo mismo que cifrar pero al reves
    for digrama in $SPLIT; 
    do
        indxDER=0
        # Ver si el diagrama no tiene una sola letra
        if [ ${#digrama} -eq 1 ]; then
            indxDER=$( index "X" )
        else
            indxDER=$( index "${digrama:1:1}" )
        fi
        # Hallar index
        indxIZQ=$( index "${digrama:0:1}" )
        indxDER=$( index "${digrama:1:1}" )
        # Veremos cual es su fila y columna
        FILA1=$(( ($indxIZQ) / 5 ))
        FILA2=$(( ($indxDER) / 5 ))
        COLUMNA1=$(( ($indxIZQ) % 5 ))
        COLUMNA2=$(( ($indxDER) % 5 ))
        
        # AMBAS FILAS
        if [ $FILA1 -eq $FILA2 ]; then
            # Shiftear a la derecha
            COLUMNA1=$(( ($COLUMNA1 + 4) % 5 ))
            COLUMNA2=$(( ($COLUMNA2 + 4) % 5 ))
        # AMBAS COLUMNAS
        elif [ $COLUMNA1 -eq $COLUMNA2 ]; then
            FILA1=$(( ($FILA1 + 4) % 5 ))
            FILA2=$(( ($FILA2 + 4) % 5 ))
        # ESQUINAS
        else
            #INTERCAMBIAMOS DE ESQUINAS, MANTENER FILA PERO CAMBIAR COLUMNA
            aux=$COLUMNA1
            COLUMNA1=$COLUMNA2
            COLUMNA2=$aux
        fi

        indxIZQ=$(( ($FILA1 * 5) + $COLUMNA1 ))
        indxDER=$(( ($FILA2 * 5) + $COLUMNA2 ))
        # Agregamos al resultado
        RESULTADO="$RESULTADO${CUADRADO[$indxIZQ]}${CUADRADO[$indxDER]}"
    done
}

# Verificar que use los argumentos
if [ $# -ne 3 ]; then
    echo "Uso: ./script.sh CLAVE OPCION MENSAJE"
    exit 1
fi

# Preprocesado
crearcuadrado $1
split "$3"

# Opciones
case $2 in
    # Cifrar
    -c | --cifrar)
    cifrar
    ;;
    # Decifrar
    -d | --decifrar)
    decifrar
    ;;
    # Otra cosa
    *)
    echo "No puso opcion"
    exit 1
esac

echo "$RESULTADO"