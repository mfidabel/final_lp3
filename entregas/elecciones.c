#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <semaphore.h>
#include <sys/shm.h>
#include <sys/stat.h>

struct info_elector
{
    int *urna;
    pthread_mutex_t mutex;
};

//Definición de lo compartido
typedef struct info_general {
    int *votos;
    sem_t *mutex_transmitir;
} info_general;

// No es necesario mutex, pues solo se lee
int cant_candidatos;
int cant_regiones;
int min_electores;
int max_electores;


//SHM
info_general *shared_memory;
int *votos;

//Spawner de regiones
pid_t spawn_region()
{
    pid_t child_pid;
    //Duplicar proceso
    child_pid = fork();
    return child_pid;
}

void *elector(void *info)
{
    //Elijo un candidato y voto
    int eleccion = rand() % cant_candidatos;
    //Grabo eso en mi region
    struct info_elector *info_urna = (struct info_elector *)info;
    int *urna = info_urna->urna;
    //Vamos a meter nuestro votito, solo una persona puede meter su votito
    //En la urna a la vez
    pthread_mutex_lock(&(info_urna->mutex));

    urna[eleccion]++;

    pthread_mutex_unlock(&(info_urna->mutex));
    //Se va a su casa el elector
}

/* USAREMOS DOS METEDOS IPC (SHARED MEMORY Y PIPE) para dos cosas distintas */
int main(int argc, char *const argv[])
{
    //Datos iniciales
    cant_candidatos = atoi(argv[1]);
    cant_regiones = atoi(argv[2]);
    min_electores = atoi(argv[3]);
    max_electores = atoi(argv[4]);

    //Memoria compartida, usaremos para transmitir los numeros, LISTING
    int segment_id, segment_id_voto;
    struct shmid_ds shmbuffer;
    int segment_size;
    const int shared_segment_size = sizeof(info_general);
    /* Allorar segmento */
    segment_id = shmget(IPC_PRIVATE, shared_segment_size,
                        IPC_CREAT | S_IRUSR | S_IWUSR);

    segment_id_voto = shmget(IPC_PRIVATE, sizeof(int)*cant_candidatos,
                        IPC_CREAT | S_IRUSR | S_IWUSR);
    shared_memory = (info_general*) shmat (segment_id, 0, 0); //la info
    votos = (int *) shmat(segment_id_voto, 0x5000000, 0); //Los votos



    //Seteamos semaforo
    sem_init(shared_memory->mutex_transmitir, 0, 1);
    

    //Seteamos los votos
    shared_memory->votos = votos;
    for (int i=0; i<cant_candidatos; i++){
        shared_memory->votos[i] = 0;
    };

    //Empezamos creando cada región
    pid_t pids[cant_candidatos];

    //Donde se transmiten los votos, es nada más para leer su reporte
    int fds[2];
    pipe(fds); //Creamos pipes para comunicar aca los resultados
    dup2(fds[0], STDIN_FILENO); //LEEREMOS DEL PIPE



    for (int i = 0; i < cant_regiones; i++)
    {
        int pid = spawn_region();

        if (pid == 0)
        {
            //Soy hijo, hago mi tema de la región
            close(fds[0]);
            dup2(fds[1], STDOUT_FILENO); //Hacemos que imprima en el pipe
            int numero = i;

            //Alocamos
            shared_memory = (info_general*) shmat (segment_id, 0, 0);

            //Seteamos semilla
            srand((long)time(NULL));
            //Primero tenemos que generar a todos los candidatos
            int electores = (rand() % (max_electores - min_electores)) + min_electores;
            //Crearemos un contador de votos
            int urna[cant_candidatos];
            for (int i = 0; i < cant_candidatos; i++)
            {
                urna[i] = 0;
            }
            //Creamos una info
            struct info_elector info;
            info.urna = urna;
            pthread_mutex_init(&(info.mutex), NULL);
            //Por cada elector creamos un hilo
            pthread_t hilos[electores];
            for (int i = 0; i < electores; i++)
            {
                pthread_create(&(hilos[i]), NULL, &elector, (void *)&info);
            }
            //Esperamos que voten todos para cerrar
            for (int i = 0; i < electores; i++)
            {
                pthread_join(hilos[i], NULL);
            }
            //Mandamos resultados
            sleep(1);
            sem_wait(shared_memory->mutex_transmitir); //Esperamos que se pueda transmitir
            printf("\nResultado Region %d: ", numero + 1);
            for (int i = 0; i < cant_candidatos; i++)
            {
                shared_memory->votos[i] += urna[i]; //El conteo
                //La impresión
                printf(" Candidato %d total de %d votos", i + 1, urna[i]);
            }
            sem_post(shared_memory->mutex_transmitir); //Libramos el pipe
            return 0;                    //Terminamos este proceso
        }
        else
        {
            pids[i] = pid;
        }
    }

    //Aca solo ejecuta el padre
    int status;

    for (int i = 0; i < cant_regiones; i++)
    {
        pid_t x = wait(&status);
    }

    //Imprimimos resultados
    printf("\nConteo general: ");
    for (int i=0; i<cant_candidatos; i++){
        printf(" Candidato %d -> %d votos ", i + 1, shared_memory->votos[i]);
    }
    

    fputs("\0", stdin); //Para que sepa donde termina
    //Imprimimos resultados de cada region en el orden que termina su urna
    for (int i = 0; i < cant_regiones; i++)
    {
        char buffer[4096];
        fgets(buffer, sizeof(buffer), stdin);
        fputs(buffer, stdout);
    }

    

    return 0;
}